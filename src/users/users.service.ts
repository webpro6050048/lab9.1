/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
// eslint-disable-next-line prettier/prettier

export type User = any;

@Injectable()
export class UsersService {
  private readonly users = [
    {
      id: 1,
      email: 'admin@mail.com',
      password: 'Pass@1234',
    },
    {
      id: 2,
      email: 'User1@mail.com',
      password: 'Pass@1234',
    },
  ];
  async findOne(email: string): Promise<User | undefined> {
    return this.users.find((user) => user.email === email);
  }
}
